# Testing GCF With Uploader

To authenticate to the google bucket, you can manually log into the client or set:

`export GOOGLE_APPLICATION_CREDENTIALS=/path/to/json/credentials`

To upload the google cloud function:

    cd gcf/
    gcloud functions deploy <function-name> --runtime python37 --trigger-resource <bucket-name> --trigger-event <event-to-trigger-from>
