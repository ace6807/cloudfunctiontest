from flask import Flask, flash, request, redirect
from werkzeug.utils import secure_filename
from google.cloud import storage
import uuid
import os

ALLOWED_EXTENSIONS = {'txt', 'json'}

app = Flask(__name__)
app.secret_key = '12345'
app.config['UPLOAD_BUCKET'] = os.environ.get('BUCKET_NAME')

storage_client = storage.Client()
bucket = storage_client.get_bucket(app.config['UPLOAD_BUCKET'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def create_unique_filename(filename):
    parts = filename.rsplit('.', 1)
    return f"{parts[0]}.{str(uuid.uuid4())}.{parts[1]}"


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            unique_filename = secure_filename(create_unique_filename(file.filename))
            blob = bucket.blob(unique_filename)
            blob.upload_from_string(file.read())
            return '<h1>File Upload Complete<h1>'
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''