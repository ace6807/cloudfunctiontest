from rethinkdb import RethinkDB


def watch_rethink():
    r = RethinkDB()
    connection = r.connect(db='dbname', host='hostip')

    for change in r.table('uploads').changes().run(connection):
        print(change)

    connection.close()


if __name__ == '__main__':
    watch_rethink()
