from datetime import datetime

from google.cloud import storage
from rethinkdb import RethinkDB

from time import sleep


def process_uploaded_file(data, context):
    """Background Cloud Function to be triggered by Cloud Storage upload.

    Args:
        data (dict): The Cloud Functions event payload.
        context (google.cloud.functions.Context): Metadata of triggering event.
    Returns:
        None; the output is written to Stackdriver Logging
    """

    client = storage.Client()
    bucket = client.bucket(data['bucket'])
    blob = bucket.blob(data['name'])

    r = RethinkDB()
    connection = r.connect(host='hostip', db='dbname')

    inserted_id = r.table('uploads').insert({
        'upload_status': 'started',
        'filename': data['name'],
        'start_time': datetime.utcnow().timestamp(),
        'time_created': data['timeCreated'],
        'event_id': context.event_id
    }).run(connection)['generated_keys'][0]

    # print('Event ID: {}'.format(context.event_id))
    # print('Event type: {}'.format(context.event_type))
    # print('Bucket: {}'.format(data['bucket']))
    # print('File: {}'.format(data['name']))
    # print('Metageneration: {}'.format(data['metageneration']))
    # print('Created: {}'.format(data['timeCreated']))
    # print('Updated: {}'.format(data['updated']))
    #
    # print(f'File contents: {blob.download_as_string()}')

    sleep(10)

    r.table('uploads').get(inserted_id).update({'upload_status': 'complete'}).run(connection)
    print("Done.")
    connection.close()

